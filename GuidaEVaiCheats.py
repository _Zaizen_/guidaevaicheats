import json
import requests
import random
import configparser
import time


print("       ____   __    __   ____  ____  __ _       ")
print("      (__  ) / _\  (  ) (__  )(  __)(  ( \      ")
print(" ____  / _/ /    \  )(   / _/  ) _) /    / ____ ")
print("(____)(____)\_/\_/ (__) (____)(____)\_)__)(____)")


print("\nProgramma scritto da me e per me. Se non va, non fate affidamento su di me.\n")

#Login e caricamento config

config = configparser.RawConfigParser()
config.read('./config.cfg')

login_dati = dict(config.items('SECTION_NAME'))

login = requests.post('https://app.guidaevai.com/o/token/', data = login_dati)

auth_key = login.json()['access_token']

header = {'Host': 'app.guidaevai.com',
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0',
          'Accept': 'application/json, text/plain, */*',
          'Accept-Language': 'it',
          'Accept-Encoding': 'gzip, deflate, br',
          'Authorization': 'Bearer ' + auth_key,
          'Origin': 'https://everest.guidaevai.com',
          'Connection': 'keep-alive',
          'Referer': 'https://everest.guidaevai.com/',
          'DNT': '1',
          'TE': 'Trailers',
          }

student_id = requests.get('https://app.guidaevai.com/api/users/', headers = header)
student_id = student_id.json()
student_id = student_id[0]['id']

#Interazione con l'utente
ripetizioni = int(input("Quanti quiz vuoi fare? "))
attesa_random = int(input("Vuoi specificare un delay fisso o un range? \n1) Fisso \n2) Range \n"))

if attesa_random == 2:
    valore_attesa_1 = int(input("Inserisci il tempo minimo tra 2 test: "))
    valore_attesa_2 = int(input("Inserisci il tempo massimo tra 2 test: "))
elif attesa_random == 1:
    valore_attesa_1 = int(input("Inserisci il tempo fisso tra 2 test: "))

percentuale_errore = int(input("Che percentuale di errore vuoi nei tuoi quiz? "))

while ripetizioni != 0 :

    import datetime

    print("\nOra verrà fatto un quiz!")

    #Orario

    orario = datetime.datetime.now()
    now_iso = orario.isoformat()

    #Variabili buffer/utility

    valori = [0, 1]
    errori = 0

    #Ottenere quesiti e creare ciò che si deve inviare

    x = requests.get('https://app.guidaevai.com/api/quizzes/generate_sheet/?license_type=11')
    y = x.json()

    post_inizio = '{"type":0,"duration":30,"number_question":40,"max_number_error":4,"quizzes":['

    post_mezzo = ''
    for x in y:
        id = x['id']
        argument = x['argument']
        risposta_corretta = x['result']
        scelta_random = random.choices(valori, weights=(percentuale_errore, 100 - percentuale_errore), k=1)
        if scelta_random[0] == 0 :
            errori = errori + 1
            risposta_corretta = not risposta_corretta

        if risposta_corretta == True:
            risposta_corretta = 1
        elif risposta_corretta == False:
            risposta_corretta = 0

        post_mezzo = post_mezzo + ',[' + str(id) + ',' + str(risposta_corretta) + ',' + str(scelta_random[0]) + ',' + str(argument) + ']'
        
    post_mezzo = post_mezzo[1:]

    is_passed = 'true'

    if errori > 4:
        is_passed = 'false'

    datetime = now_iso[0:10] + 'T' + str(int(now_iso[11:13]) - 2) + ':' + now_iso[14:23] + 'Z'

    post_fine = '],"student":' + str(student_id) + ',"license_type":11,"is_executed":true,"start_datetime":"' + datetime + '","end_datetime":"' + datetime + '","is_passed":'+is_passed+',"number_correct_question":' + str((40 - errori)) + ',"number_error_question":'+ str(errori) + ',"number_empty_question":0,"os":2}'

    post_totale = post_inizio + post_mezzo + post_fine
    post_totale_json = json.loads(post_totale)

    finale = requests.post('https://app.guidaevai.com/api/sheets/', json = post_totale_json, headers = header)
    print("\nQuiz Terminato!")

    ripetizioni = ripetizioni - 1

    if ripetizioni != 0 :
        print("\nAttendo il tempo prestabilito...")
        if attesa_random == 2:
            attesa = random.randint(valore_attesa_1, valore_attesa_2)
        elif attesa_random == 1:
            attesa = valore_attesa_1
        
        time.sleep(attesa)
